import mysql.connector
import csv

host = "localhost"
user = "root"
password = "skathrzira1042"
database = "weather"

def openConnection():
    try:
        # Establish the database connection
        connection = mysql.connector.connect(
            host=host,
            user=user,
            password=password,
            database=database
        )

        if connection.is_connected():
            print("Connected to MySQL database")
    except mysql.connector.Error as e:
        print(f"Error: {e}")

    return(connection)

def closeConnection(connection):
    if 'connection' in locals() and connection.is_connected():
        connection.close()
        print("Connection closed")

class WeatherData:
    def __init__(self, date, location, temperature, humidity, condition, weather_type='blank'):
        self._date = None
        self._location = None
        self._temperature = None
        self._humidity = None
        self._condition = None
        
        if date:
            self.date = date
        else:
            self.date = None
        if location:
            self.location = location.strip()
        else:
            self.location = None
        if temperature and temperature.strip().isnumeric():
            self.temperature = int(temperature)
        else:
            self.temperature = None
        if humidity and humidity.strip().isnumeric():
            self.humidity = int(humidity)
        else:
            self.humidity = None
        if condition:
            self.condition = condition.strip()
        else:
            self.condition = None
        if self.temperature == None:
            self.weather_type = None
        elif self.temperature > 75:
            self.weather_type = 'Warm'
        elif self.temperature < 59:
            self.weather_type = 'Cold'
        else:
            self.weather_type = 'Moderate'
        self.temperature = (int(temperature) - 32) * 5.0/9.0
    

def create_database(cursor):
    cursor.execute('CREATE DATABASE IF NOT EXISTS WeatherTechDB')
    cursor.execute('USE WeatherTechDB;\
    CREATE TABLE IF NOT EXISTS WeatherData (\
    id INT AUTO_INCREMENT PRIMARY KEY,\
    date DATE NOT NULL,\
    location VARCHAR(255) NOT NULL,\
    temperature DECIMAL(5, 2) NOT NULL,\
    humidity DECIMAL(5, 2) NOT NULL,\
    weathercondition VARCHAR(255) NOT NULL,\
    weathertype VARCHAR(255) NOT NULL);')

def fetch_weather_data(connection, city=None, start_date=None, end_date=None):

    cursor = connection.cursor()

    # Build the query based on provided parameters
    query = "SELECT temperature, humidity FROM WeatherData WHERE 1=1"
    if city:
        query += f" AND location = '{city}'"
    if start_date:
        query += f" AND date >= '{start_date}'"
    if end_date:
        query += f" AND date <= '{end_date}'"

    # Execute the query
    cursor.execute(query)

    # Fetch all the rows
    data = cursor.fetchall()

    # Calculate average temperature and humidity
    total_temperature = 0
    total_humidity = 0
    num_entries = len(data)

    for temperature, humidity in data:
        total_temperature += temperature
        total_humidity += humidity

    avg_temperature = total_temperature / num_entries
    avg_humidity = total_humidity / num_entries
    
    with open("filtered_weather_data.csv", mode="w", newline="") as csvfile:
        csvwriter = csv.writer(csvfile)
        
        # Write header row
        csvwriter.writerow(["Date", "Location", "Temperature", "Humidity", "Condition", "WeatherType"])
        
        # Write data rows
        csvwriter.writerows([avg_temperature, avg_humidity])
    
if __name__ == "__main__":
    connection = openConnection()
    cursor = connection.cursor()
    #create_database(cursor)
    filename = "weather-data.txt"  # Replace with your CSV file's name
    try:
        with open(filename, 'r', newline='') as file:
            reader = csv.reader(file)
            next(reader) #skip header line
            
            for line in file:
                data = line.split(',')
                weather = WeatherData(data[0], data[1], data[2], data[3], data[4])
                data = {
                        "date": weather.date,
                        "location": weather.location,
                        "temperature": weather.temperature,
                        "humidity": weather.humidity,
                        "condition": weather.condition,
                        "weathertype": weather.weather_type
                        }
                insert_query = """
                            INSERT INTO WeatherData (date, location, temperature, humidity, condition, weathertype)
                            VALUES (%(date)s, %(location)s, %(temperature)s, %(humidity)s, %(condition)s, %(weathertype)s)
                            """
                cursor.execute(insert_query)
        city_name = "city name"
        start_date = "start date"
        end_date = "end date"
        fetch_weather_data(cityname, start_date, end_date)
                
    except FileNotFoundError:
        print(f"File not found: {filename}")
    finally:
        closeConnection(connection)